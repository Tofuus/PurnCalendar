package com.tofuus.purncalendar;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class CalendarUtil {

    PurnCalendar plugin;

    public CalendarUtil(PurnCalendar plugin) {
        this.plugin = plugin;
    }

    public void sendCalendar(Player player) {
        String message = plugin.getConfig().getString("calendarMessage");

        message = replacePlaceholders(message);

        player.sendMessage(message);
        if (plugin.getConfig().getBoolean("sendMessageOnHoldiay") && plugin.getCalendar().isHoliday()) {
            player.sendMessage(ChatColor.GOLD + "It's " + plugin.getCalendar().getHoliday() + "!");
        }
    }

    public void sendNewDayCalendar(Player player) {
        String message = plugin.getConfig().getString("newDayMessage");

        message = replacePlaceholders(message);

        player.sendMessage(message);
        if (plugin.getConfig().getBoolean("sendMessageOnHoldiay") && plugin.getCalendar().isHoliday()) {
            player.sendMessage(ChatColor.GOLD + "It's " + plugin.getCalendar().getHoliday() + "!");
        }
    }

    private String replacePlaceholders(String message) {
        message = message
                .replace("%d", plugin.getCalendar().getDay())
                .replace("%n", String.valueOf(plugin.getCalendar().getNumericalDay()))
                .replace("%m", plugin.getCalendar().getMonth())
                .replace("%y", String.valueOf(plugin.getCalendar().getYear()));
        message = ChatColor.translateAlternateColorCodes('&', message);

        return message;
    }

    public String ordinal(int i) {
        return i % 100 == 11 || i % 100 == 12 || i % 100 == 13 ? i + "th" : i + new String[]{"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"}[i % 10];
    }
}
