package com.tofuus.purncalendar;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CalendarCommands implements CommandExecutor {

    PurnCalendar plugin;

    public CalendarCommands(PurnCalendar plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("calendar")) {
            if (sender instanceof Player) {
                plugin.getCalendarUtil().sendCalendar((Player) sender);
            }
        } else if (command.getName().equalsIgnoreCase("purncalendar")) {
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("reload")) {
                    plugin.getCalendar().load();
                } else if (args[0].equalsIgnoreCase("debug")) {
                    plugin.getCalendar().debug();
                }
            } else if (args.length == 3) {

            }
        }
        return false;
    }
}
