package com.tofuus.purncalendar;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

public final class PurnCalendar extends JavaPlugin implements Listener {

    private Calendar calendar;
    private CalendarUtil calendarUtil;
    private BukkitTask task;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        calendar = new Calendar(this);
        calendarUtil = new CalendarUtil(this);

        CalendarCommands calendarCommands = new CalendarCommands(this);
        getCommand("calendar").setExecutor(calendarCommands);
        getCommand("purncalendar").setExecutor(calendarCommands);

        getServer().getPluginManager().registerEvents(this, this);

        task = getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                calendar.advanceDay(System.currentTimeMillis());
            }
        }, 0L, 1200L); // Runs every minute
    }

    @Override
    public void onDisable() {
        task.cancel();
        calendar.onDisable();
    }

    public CalendarUtil getCalendarUtil() {
        return calendarUtil;
    }

    public Calendar getCalendar() {
        return calendar;
    }


    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (getConfig().getBoolean("sendCalendarOnJoin")) {
            calendarUtil.sendCalendar(event.getPlayer());
        }
    }
}