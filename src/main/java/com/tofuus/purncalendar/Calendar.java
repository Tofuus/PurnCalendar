package com.tofuus.purncalendar;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class Calendar {

    private PurnCalendar plugin;

    private ConcurrentHashMap<Integer, String> monthNames;
    private ConcurrentHashMap<Integer, Integer> monthDays;
    private ConcurrentHashMap<Integer, String> dayNames;
    private ConcurrentHashMap<String, String> holidays;
    private ConcurrentHashMap<String, String> holidayNames;

    private int dayLength;
    private int yearLength;

    private int currentHour;
    private int currentDay;
    private int currentMonth;
    private int currentYear;
    private long milliSinceLastHour;

    public Calendar(PurnCalendar plugin) {
        this.plugin = plugin;

        load();
    }

    /**
     * Calls all the load methods.
     */
    public void load() {
        monthNames = loadConfigSection("months");
        dayNames = loadConfigSection("days");
        loadMonthDays();
        loadHolidays();
        loadData();

        milliSinceLastHour = plugin.getConfig().getLong("milliSinceLastHour", System.currentTimeMillis());

        dayLength = plugin.getConfig().getInt("length.day");
        yearLength = plugin.getConfig().getConfigurationSection("months").getKeys(false).size();
    }


    /**
     * Loads a configuration section.
     *
     * @param path
     *         Config path
     *
     * @return Hashmap of said configuration section
     */
    private ConcurrentHashMap<Integer, String> loadConfigSection(String path) {
        ConcurrentHashMap<Integer, String> hashmap = new ConcurrentHashMap<>();

        for (String name : plugin.getConfig().getConfigurationSection(path).getKeys(false)) {
            hashmap.put(plugin.getConfig().getInt(path + "." + name + ".number"), name);
        }

        return hashmap;
    }

    private void loadMonthDays() {
        monthDays = new ConcurrentHashMap<>();

        for (String month : plugin.getConfig().getConfigurationSection("months").getKeys(false)) {
            int number = plugin.getConfig().getInt("months." + month + ".number");
            int days = plugin.getConfig().getInt("months." + month + ".days");
            int defaultDays = plugin.getConfig().getInt("length.month", 30);

            // If days = 0 that means there was the length of a month was not specified. Default to length.month
            if (days != 0) {
                monthDays.put(number, days);
            } else {
                monthDays.put(number, defaultDays);
            }
        }
    }

    private void loadHolidays() {
        holidays = new ConcurrentHashMap<>();
        for (String date : plugin.getConfig().getConfigurationSection("holidays").getKeys(false)) {
            holidays.put(date, plugin.getConfig().getString("holidays." + date + ".message"));
        }
    }

    private void loadData() {
        currentHour = plugin.getConfig().getInt("currentHour", 0);
        currentDay = plugin.getConfig().getInt("currentDay", 0);
        currentMonth = plugin.getConfig().getInt("currentMonth", 0);
        currentYear = plugin.getConfig().getInt("currentYear", 0);
    }

    public String getDay() {
        return dayNames.get(currentDay);
    }

    public int getNumericalDay() {
        return currentDay;
    }

    public String getMonth() {
        return monthNames.get(currentMonth);
    }

    public int getYear() {
        return currentYear;
    }

    private int getMonthLength(int month) {
        return monthDays.get(month);
    }

    public String getHoliday() {
        return holidayNames.get(currentDay + "/" + currentMonth);
    }

    public boolean isHoliday() {
        return holidays.contains(currentDay + "/" + currentMonth);
    }

    /**
     * Core loop for calendar
     */
    public void advanceDay(long currentMillis) {
        long timePassed = TimeUnit.MILLISECONDS.toHours(currentMillis - milliSinceLastHour);

        if (timePassed >= 1) {
            currentHour++;
            milliSinceLastHour = currentMillis;
            if (currentHour > dayLength) {
                currentHour = 1;
                currentDay++;

                if (plugin.getConfig().getBoolean("sendCalendarOnNewDay")) {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        plugin.getCalendarUtil().sendNewDayCalendar(player);
                    }

                }

                if (currentDay > getMonthLength(currentMonth)) {
                    currentMonth = 1;
                    currentMonth++;
                    if (currentMonth > yearLength) {
                        currentMonth = 1;
                        currentYear++;
                    }
                }
            }
        }


        String today = currentDay + "/" + currentMonth;
        String message = holidays.get(today);
        if (plugin.getConfig().getBoolean("sendMessageOnHoldiay") && isHoliday() && message != null) {
            Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', message));
        }

    }

    /**
     * Quick debug method that is super hacky. Advances day by an hour
     */
    public void debug() {
        long hour = 3600000;
        // Advances day by one hour
        advanceDay(milliSinceLastHour + (hour));
        // Sets milliSinceLastHour back so nothing breaks.
        milliSinceLastHour -= hour;
    }

    /**
     * Save data to the config.
     */
    public void onDisable() {
        plugin.getConfig().set("currentHour", currentHour);
        plugin.getConfig().set("currentDay", currentDay);
        plugin.getConfig().set("currentMonth", currentMonth);
        plugin.getConfig().set("currentYear", currentYear);
    }
}
